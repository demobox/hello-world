#!/usr/bin/env sh

echo "Checking if the webserver returns the expected response..."
if (curl -s www | grep -q -- "Hello World"); then
    echo "Tests pass!"
else
    echo "Tests fail!"

    exit 1
fi