hello-world
===========

Launch tests:

```
docker-compose -f docker-compose-testing.yml stop && \
docker-compose -f docker-compose-testing.yml rm -fv && \
docker-compose -f docker-compose-testing.yml build && \
docker-compose -f docker-compose-testing.yml run --rm testing
```
